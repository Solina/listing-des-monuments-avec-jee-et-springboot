package com.fetima.prjetjee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.fetima.prjetjee.dao.DepartementDaoImpl;

import com.fetima.prjetjee.entities.Departement;
@Controller
@RequestMapping("/departement")
public class DepartementController {

	@Autowired
	private DepartementDaoImpl DepartementDAO;
	
	@RequestMapping(value ="/ListDepartements")
	public String afficheDepListe(Model model) {
		
		List<Departement> dps = DepartementDAO.findAll();
		model.addAttribute("ListDepartements", dps);
		return "ListDepartements";
	}
	
	@RequestMapping(value ="/formulaireDepartement", method = RequestMethod.GET)
	public String addDPT(Model model) {
		model.addAttribute("formulaireDepartement", new Departement());
		return "AddDept";
	}
	
	
	@PostMapping(value ="/AddDept")
	public String addDPT(Model model, @ModelAttribute("formulaireDepartement") Departement depAAjouter) {
		DepartementDAO.save(depAAjouter);
		
		List<Departement> dps = DepartementDAO.findAll();
		model.addAttribute("ListDepartements", dps);
		return "ListDepartements";
	}
	
	@RequestMapping(value ="/SuppDept", method = RequestMethod.GET)
	public String afficheDepListes(Model model) {
		
		List<Departement> dps = DepartementDAO.findAll();
		model.addAttribute("dptss", dps);
		return "SuppDept";
	}
	
	@RequestMapping(value="/supprimer", method = RequestMethod.GET)
	public String deletedep( String dep) {
		DepartementDAO.delete(dep);
		
		return "redirect:SuppDept";
	}

}


