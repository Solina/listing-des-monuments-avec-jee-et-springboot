package com.fetima.prjetjee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fetima.prjetjee.dao.LieuDaoImpl;
import com.fetima.prjetjee.entities.Lieu;

@Controller
@RequestMapping("/lieu")
public class LieuController {
	@Autowired
	private LieuDaoImpl lieuDao;

	@RequestMapping(value = "/ListLieux", method = RequestMethod.GET)
	public String afficheLieu(Model model) {
		List<Lieu> llx = lieuDao.findAll();
		model.addAttribute("ListLieux", llx);
		return "ListLieux";

	}

	@RequestMapping(value = "/SuppLieu", method = RequestMethod.GET)
	public String afficheLieus(Model model) {
		List<Lieu> llx = lieuDao.findAll();
		model.addAttribute("supplieux", llx);
		return "suppLieu";
	}

	@RequestMapping(value = "/supprimer", method = RequestMethod.GET)
	public String deleteLieu(String codeInsee) {
		lieuDao.delete(codeInsee);
		return "redirect:SuppLieu";
	}

	@RequestMapping(value = "/formulaireLieu", method = RequestMethod.GET)
	public String AddLux(Model model) {
		model.addAttribute("formulaireLieu", new Lieu());
		return "AddLieu";
	}

	@PostMapping(value = "/AddLieu")
	public String AddLux(Model model, @ModelAttribute("formulaireLieu") Lieu lieuAAjouter) {

		lieuDao.save(lieuAAjouter);

		List<Lieu> llx = lieuDao.findAll();
		model.addAttribute("ListLieux", llx);
		return "ListLieux";
	}

}
