package com.fetima.prjetjee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fetima.prjetjee.dao.CelebriteDaoImpl;
import com.fetima.prjetjee.entities.Celebrite;
@Qualifier("CelebriteDaoImpl")

@Controller
@RequestMapping("/celebrite")

public class CelebriteController {
	@Autowired
	private CelebriteDaoImpl celebriteDAO;
	
	
	

	/*Ici mon objet se nomme cls mais j'ai nommé par la suite clts l'attribut 
	qui contient cet objet dans la requête. Côté vue, 
	c'est par ce nom d'attribut que je pourrai accéder à mon objet !*/
	
	
	
	@RequestMapping(value ="/ListCelebrites", method = RequestMethod.GET)
	public String afficheCelListe(Model model) {
		
		List<Celebrite> cls = celebriteDAO.findAll();
		model.addAttribute("Celebrites", cls);
		return "ListCelebrites";
	}
	
	@RequestMapping(value ="/formulaireCelebrite", method = RequestMethod.GET)
	public String addCLB(Model model) {
		model.addAttribute("formulaireCelebrite", new Celebrite());
		return "AddCeleb";
		
	}
	@PostMapping(value ="AddCeleb")
	public String AddCLB(Model model, @ModelAttribute("formulaireCelebrite") Celebrite celAAjouter) {
		celebriteDAO.save(celAAjouter);
		
		List<Celebrite> cls = celebriteDAO.findAll();
		model.addAttribute("Celebrites", cls);
		return "ListCelebrites";
		
		
	}
	
	@RequestMapping(value ="/SuppCelebrite", method = RequestMethod.GET)
	public String afficheCellList(Model model) {
		
		List<Celebrite> cls = celebriteDAO.findAll();
		model.addAttribute("Celebrites", cls);
		return "SuppCelebrite";
	}
	
	@RequestMapping(value="/supprimer", method = RequestMethod.GET)
	public String deleteCeleb(String numCelebrite) {
	       celebriteDAO.delete(numCelebrite);
	       
		return "redirect:SuppCelebrite";

    }
}


