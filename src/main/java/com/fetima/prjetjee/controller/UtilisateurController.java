package com.fetima.prjetjee.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fetima.prjetjee.dao.UtilisateurDao;
import com.fetima.prjetjee.entities.Utilisateur;
import com.fetima.prjetjee.security.AccesSecurity;


@Controller
public class UtilisateurController {
	@Autowired
	private UtilisateurDao utilisateurDao;
    private AccesSecurity accessSecurity = new AccesSecurity();
	
	@RequestMapping(value ="/", method = RequestMethod.GET)
	public String navCreate() {		
		return "index";
	}

	
	
	@RequestMapping(value="/Login")
	public String loginDefault(Model model){
		Utilisateur user = new Utilisateur();
		model.addAttribute("userForm", user);
		return "Login";
	}
	
	
	@RequestMapping(value="/Accueil", method = RequestMethod.POST)
	public String checkLogin(@ModelAttribute("registerForm") Utilisateur userReceived, HttpSession session, Model model){
		boolean error = false;
		Utilisateur logUser = utilisateurDao.getUser(userReceived);   
		if (logUser != null) {
			session.setAttribute("USER", logUser);
			return "Accueil";
		}
		error = true;
		model.addAttribute("error", error);
		
		
		return "Login";
	}
	
	
	@RequestMapping(value ="/register", method = RequestMethod.GET)
	public String addDPT(Model model) {
		model.addAttribute("registerForm", new Utilisateur());
		return "SignUP";
	}
	
	@PostMapping(value ="/SignUP")
	public String addDPT(Model model, @ModelAttribute("registerForm") Utilisateur newUser) {
		utilisateurDao.addUtilisateur(newUser);		
		return "redirect:/Login";
	}


}
