package com.fetima.prjetjee.controller;

	import java.util.List;

	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Controller;
	import org.springframework.ui.Model;
	import org.springframework.web.bind.annotation.DeleteMapping;
	import org.springframework.web.bind.annotation.ModelAttribute;
	import org.springframework.web.bind.annotation.PathVariable;
	import org.springframework.web.bind.annotation.PostMapping;
	import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RequestMethod;
	import org.springframework.web.bind.annotation.RequestParam;

    import com.fetima.prjetjee.dao.MonumentDaoImpl;
    import com.fetima.prjetjee.entities.Monument;

	

	@Controller
	@RequestMapping("/monument")
	public class MonumentController {
		
		@Autowired
		private MonumentDaoImpl moda;

		@RequestMapping(value= "ListMonuments", method = RequestMethod.GET)
		public String afficheMonListe(Model model) { 
			
			List<Monument> lmx = moda.findAll();
			model.addAttribute("ListMonuments", lmx);
			return "ListMonuments";
			
		}
		
		 @RequestMapping(value="/suppMonument", method = RequestMethod.GET)
		public String afficheMon(Model model) {
			List<Monument> lmx = moda.findAll();
			model.addAttribute("ListMonuments", lmx);
			return "suppMonument";
		}
		@RequestMapping(value="/supprimer", method = RequestMethod.GET)
		public String deleteLieu(String codeM) {
			moda.delete(codeM);
			return "redirect:suppMonument";
		}
		
		
		@RequestMapping(value ="/formulaireMonument", method = RequestMethod.GET)
		public String AddM(Model model) {
			model.addAttribute("formulaireMonument", new Monument());
			return "AddMonu";
		}
		
		@PostMapping(value="/AddMonu")
		public String AddM(Model model, @ModelAttribute("formulaireMonument") Monument monumentAAjouter) {
			//System.out.print(monumentAAjouter);
			
			moda.save(monumentAAjouter);
			List<Monument> lmx=moda.findAll();
			model.addAttribute("ListMonuments", lmx);
			return "ListMonuments";
			
		
		}

}
