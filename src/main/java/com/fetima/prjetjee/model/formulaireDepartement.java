package com.fetima.prjetjee.model;

import java.util.Collection;

import com.fetima.prjetjee.entities.Lieu;

public class formulaireDepartement {
	private String numDep;
	private String chefLieu;
	private String nomDep;
	private String region;
	private Collection<Lieu> lieux;
	public formulaireDepartement() {
		super();
		// TODO Auto-generated constructor stub
	}
	public formulaireDepartement(String numDep, String chefLieu, String nomDep, String region, Collection<Lieu> lieux) {
		super();
		this.numDep = numDep;
		this.chefLieu = chefLieu;
		this.nomDep = nomDep;
		this.region = region;
		this.lieux = lieux;
	}
	public String getNumDep() {
		return numDep;
	}
	public String getChefLieu() {
		return chefLieu;
	}
	public String getNomDep() {
		return nomDep;
	}
	public String getRegion() {
		return region;
	}
	public Collection<Lieu> getLieux() {
		return lieux;
	}
	public void setNumDep(String numDep) {
		this.numDep = numDep;
	}
	public void setChefLieu(String chefLieu) {
		this.chefLieu = chefLieu;
	}
	public void setNomDep(String nomDep) {
		this.nomDep = nomDep;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public void setLieux(Collection<Lieu> lieux) {
		this.lieux = lieux;
	}
	

}
