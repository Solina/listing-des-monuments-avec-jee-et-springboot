package com.fetima.prjetjee.model;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fetima.prjetjee.entities.Lieu;

public class formulaireMonument {
	private String codeM;
	private String nomM;
	private String proprietaire;
	private String typeMonument;
	private float latitude;
	private float longitude;
	private String localite;
	public formulaireMonument() {
		super();
		// TODO Auto-generated constructor stub
	}
	public formulaireMonument(String codeM, String nomM, String proprietaire, String typeMonument, float latitude,
			float longitude, String localite) {
		super();
		this.codeM = codeM;
		this.nomM = nomM;
		this.proprietaire = proprietaire;
		this.typeMonument = typeMonument;
		this.latitude = latitude;
		this.longitude = longitude;
		this.localite = localite;
	}
	public String getCodeM() {
		return codeM;
	}
	public String getNomM() {
		return nomM;
	}
	public String getProprietaire() {
		return proprietaire;
	}
	public String getTypeMonument() {
		return typeMonument;
	}
	public float getLatitude() {
		return latitude;
	}
	public float getLongitude() {
		return longitude;
	}
	public String getLocalite() {
		return localite;
	}
	public void setCodeM(String codeM) {
		this.codeM = codeM;
	}
	public void setNomM(String nomM) {
		this.nomM = nomM;
	}
	public void setProprietaire(String proprietaire) {
		this.proprietaire = proprietaire;
	}
	public void setTypeMonument(String typeMonument) {
		this.typeMonument = typeMonument;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	public void setLocalite(String localite) {
		this.localite = localite;
	}
	
	
}	