package com.fetima.prjetjee.model;

public class formulaireLieu {
	private String codeInsee;
	private String nomCom;
	private float latitude;
	private float longitude;
	private String dep;
	public formulaireLieu() {
		super();
		// TODO Auto-generated constructor stub
	}
	public formulaireLieu(String codeInsee, String nomCom, float latitude, float longitude, String dep) {
		super();
		this.codeInsee = codeInsee;
		this.nomCom = nomCom;
		this.latitude = latitude;
		this.longitude = longitude;
		this.dep = dep;
	}
	public String getCodeInsee() {
		return codeInsee;
	}
	public String getNomCom() {
		return nomCom;
	}
	public float getLatitude() {
		return latitude;
	}
	public float getLongitude() {
		return longitude;
	}
	public String getNumDep() {
		return dep;
	}
	public void setCodeInsee(String codeInsee) {
		this.codeInsee = codeInsee;
	}
	public void setNomCom(String nomCom) {
		this.nomCom = nomCom;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	public void setDep(String dep) {
		this.dep = dep;
	}
	

}
