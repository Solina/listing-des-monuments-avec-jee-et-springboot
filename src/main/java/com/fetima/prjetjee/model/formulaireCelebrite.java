package com.fetima.prjetjee.model;

public class formulaireCelebrite {
	private Integer numCelebrite;
	private String nom;
	private String prenom;
	private String nationalite;
	private String epoque;
	public formulaireCelebrite() {
		super();
		// TODO Auto-generated constructor stub
	}
	public formulaireCelebrite(Integer numCelebrite, String nom, String prenom, String nationalite, String epoque) {
		super();
		this.numCelebrite = numCelebrite;
		this.nom = nom;
		this.prenom = prenom;
		this.nationalite = nationalite;
		this.epoque = epoque;
	}
	public Integer getNumCelebrite() {
		return numCelebrite;
	}
	public String getNom() {
		return nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public String getNationalite() {
		return nationalite;
	}
	public String getEpoque() {
		return epoque;
	}
	public void setNumCelebrite(Integer numCelebrite) {
		this.numCelebrite = numCelebrite;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}
	public void setEpoque(String epoque) {
		this.epoque = epoque;
	}


}
