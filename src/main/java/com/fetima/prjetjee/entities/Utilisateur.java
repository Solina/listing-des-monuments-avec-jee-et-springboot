package com.fetima.prjetjee.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity

public class Utilisateur implements Serializable {
	@Id	
	@Column (length=30)
	private String email;

    private String nom;
    private String prenom;
    private String password;
	private String role;
	public Utilisateur() {
		super();
		// TODO Auto-generated constructor stub
		
	}
	public Utilisateur(String email, String nom, String prenom, String password, String role) {
		super();
		this.email = email;
		this.nom = nom;
		this.prenom = prenom;
		this.password = password;
		this.role = role;
	}
	public String getEmail() {
		return email;
	}
	public String getNom() {
		return nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public String getPassword() {
		return password;
	}
	public String getRole() {
		return role;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setRole(String role) {
		this.role = role;
	}
	

}
