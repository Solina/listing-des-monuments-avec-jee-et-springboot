package com.fetima.prjetjee.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Monument implements Serializable {
	 /**
	 * 
	*/
	 
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(length=255)
	private String codeM;
	private String nomM;
	private String proprietaire;
	private String typeMonument;
	private float latitude;
	private float longitude;
	 @ManyToOne
	 @JoinColumn(name="FK_CodeInsee")
	private Lieu localite;
	
	public Monument() {
		super();
	}

	public Monument(String codeM, String nomM, String proprietaire, String typeMonument, float latitude, float longitude,
			Lieu localite) {
		super();
		this.codeM = codeM;
		this.nomM = nomM;
		this.proprietaire = proprietaire;
		this.typeMonument = typeMonument;
		this.latitude = latitude;
		this.longitude = longitude;
		this.localite = localite;
	}
	

	public String getCodeM() {
		return codeM;
	}

	public void setCodeM(String codeM) {
		this.codeM = codeM;
	}

	public String getNomM() {
		return nomM;
	}
	public void setNomM(String nomM) {
		this.nomM = nomM;
	}
	public String getProprietaire() {
		return proprietaire;
	}
	public void setProprietaire(String proprietaire) {
		this.proprietaire = proprietaire;
	}
	public float getLatitude() {
		return latitude;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	public float getLongitude() {
		return longitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public String getTypeMonument() {
		return typeMonument;
	}

	public void setTypeMonument(String typeMonument) {
		this.typeMonument = typeMonument;
	}
	public Lieu getLocalite() {
		return localite;
	}
	public void setLocalite(Lieu codeInsee) {
		this.localite = localite;
	}
	
}
