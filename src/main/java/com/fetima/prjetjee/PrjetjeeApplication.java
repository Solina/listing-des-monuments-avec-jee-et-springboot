package com.fetima.prjetjee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.fetima.prjetjee.dao.EntityRepository;
import com.fetima.prjetjee.entities.Celebrite;
import com.fetima.prjetjee.entities.Departement;
import com.fetima.prjetjee.entities.Monument;

@SpringBootApplication

public class PrjetjeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrjetjeeApplication.class, args);
		/*ApplicationContext ctx = SpringApplication.run(PrjetjeeApplication.class, args);// demarre spring et la classe PrjetjeeApplication, donne le context de spring
		EntityRepository<Departement> DepartemntDao=ctx.getBean(EntityRepository.class);// on utlise get bean a chaque fos q'on veut un objet.
		
		DepartemntDao.save(new Departement ("34", "Montpellier","Herault","Occitanie"));*/
		
		
		
		/*ApplicationContext ctx = SpringApplication.run(PrjetjeeApplication.class, args);// demarre spring et la classe PrjetjeeApplication, donne le context de spring
		EntityRepository<Celebrite> CelebriteDao=ctx.getBean(EntityRepository.class);// on utlise get bean a chaque fos q'on veut un objet.
		
		
		
		CelebriteDao.save(new Celebrite("Napoléon", "Bonaparte","Royale","Française"));*/
	}
}
