package com.fetima.prjetjee.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.fetima.prjetjee.entities.Monument;

@Service
@Repository // pour dire à spring que cette classe devra etre instanciée par spring
@Transactional // déleguer les transactions à spring (commit and rollback)

public class MonumentDaoImpl implements EntityRepository<Monument> { // là on gere les monuments au lieu de T, puis on
																		// importe les méthodes

	@PersistenceContext // en utilisant cette unité de persistance on demande à spring un objet em pour
						// gerer les entités
	private EntityManager em; // interface declarer

	@Override
	public Monument save(Monument m) {
		em.persist(m); // faire insert into et retourne le monument insré et l'ajoute
		return m;
	}

	@Override
	public List<Monument> findAll() {
		Query req = em.createQuery("select m from Monument m");
		return req.getResultList();
	}

	@Override
	public Monument findOne(String codeM) { // chercher un monument en sachant sa clé primaire
		Monument m = em.find(Monument.class, codeM);
		return m;
	}

	@Override
	public Monument update(Monument m) {
		em.merge(m);
		return m;
	}

	@Override
	public void delete(String codeM) {
		Monument m = em.find(Monument.class, codeM);
		em.remove(m);

	}

	@Override
	public Monument get(String codeM) {
		Monument m = em.find(Monument.class, codeM);

		return m;
	}
}
