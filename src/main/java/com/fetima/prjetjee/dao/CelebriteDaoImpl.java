package com.fetima.prjetjee.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.fetima.prjetjee.entities.Celebrite;

@Service
@Repository                                                                // pour dire à spring que cette classe devra etre instanciée par spring
@Transactional

public class CelebriteDaoImpl implements EntityRepository<Celebrite>{ ////////////////////////j'ap aps pu declarer la cle primaire en tant que Integer
	@PersistenceContext 
	private EntityManager em ;

	

	@Override
	public Celebrite save(Celebrite c) {
		em.persist(c);
		return c;
	}

	@Override
	public List<Celebrite> findAll() {
		Query req=em.createQuery("select c from Celebrite c ");
		return req.getResultList();
		
	}

	@Override
	public Celebrite findOne(String numCelebrite) {
		Celebrite c= em.find(Celebrite.class, numCelebrite);
		return c;
	}

	@Override
	public Celebrite update(Celebrite c) {
		em.merge(c);
		return c;
	}

	@Override
	public void delete(String numCelebrite) {
		Celebrite c= em.find(Celebrite.class, numCelebrite);
		em.remove(c);
		
	}

	@Override
	public Celebrite get(String numCelebrite) {
		Celebrite c= em.find(Celebrite.class, numCelebrite);
		
		return c;
	}

	


}
