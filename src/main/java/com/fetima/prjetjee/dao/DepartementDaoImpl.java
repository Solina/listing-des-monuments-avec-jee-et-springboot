package com.fetima.prjetjee.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.fetima.prjetjee.entities.Departement;

@Service
@Repository                                                                // pour dire à spring que cette classe devra etre instanciée par spring
@Transactional

public class DepartementDaoImpl implements EntityRepository<Departement>{
	@PersistenceContext
	private EntityManager em;

	@Override
	public Departement save(Departement d) {
		em.persist(d);                                                  // faire insert into et retourne le monument insré et l'ajoute
		return d;
	}

	@Override
	public List<Departement> findAll() {
		Query req=em.createQuery("select d from Departement d");
		return req.getResultList();
	}

	@Override
	public Departement findOne(String NumDep) {
		Departement d= em.find(Departement.class, NumDep);
		return d;
	}

	@Override
	public Departement update(Departement d) {
		em.merge(d);
		return d;
	}

	@Override
	public void delete(String NumDep) {
		Departement d= em.find(Departement.class, NumDep);
		em.remove(d);
		
	}
	@Override
	public Departement get(String NumDep) {
		Departement d = em.find(Departement.class,NumDep);
		
		return d;
	}

}