package com.fetima.prjetjee.dao;
import javax.persistence.EntityManager;

import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.fetima.prjetjee.entities.Utilisateur;

@Transactional
@Service

public class UtilisateurDao {
	@PersistenceContext
	private EntityManager em;
	

    public Utilisateur getUser(Utilisateur userReceived) {
    	Utilisateur newLoggedUser = new Utilisateur();
            newLoggedUser = em.find(Utilisateur.class, userReceived.getEmail());
            if (newLoggedUser == null) {
                System.out.println("le champ est null");
//                    throw new RuntimeException("Compte introuvable");
                return null;
            }
                       
            if (!userReceived.getPassword().equals(newLoggedUser.getPassword())) {
//                    throw new RuntimeException("Erreur de mot de passe !");
                return null;
            }
            else return newLoggedUser;
           
    }

    
    public Utilisateur addUtilisateur(Utilisateur newUser) {
    	Utilisateur userExistant= em.find(Utilisateur.class, newUser.getEmail());
        if (userExistant!=null) {
            throw new RuntimeException("Ce compte éxiste déjà");
        }
        else {
        	em.persist(newUser);
            return newUser;
        }           
    }
	

}
