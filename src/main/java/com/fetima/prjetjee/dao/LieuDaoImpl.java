package com.fetima.prjetjee.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.fetima.prjetjee.entities.Lieu;
import com.fetima.prjetjee.entities.Monument;

@Service
@Repository
@Transactional

public class LieuDaoImpl implements EntityRepository<Lieu> {
	@PersistenceContext
	private EntityManager em ; 

	@Override
	public Lieu save(Lieu l) {
		em.persist(l);                                                  // faire insert into et retourne le monument insré et l'ajoute
		return l;
		
	}

	@Override
	public List<Lieu> findAll() {
		Query req=em.createQuery("select l from Lieu l");
		return req.getResultList();
	}

	@Override
	public Lieu findOne(String codeInsee) {
		Lieu l= em.find(Lieu.class, codeInsee);
		return l;
	}

	@Override
	public Lieu update(Lieu l) {
		em.merge(l);
		return l;
	}

	@Override
	public void delete(String codeInsee) {
		Lieu l= em.find(Lieu.class, codeInsee);
		em.remove(l);	
	
		
	}
	@Override
	public Lieu get(String codeInsee) {
    Lieu l = em.find(Lieu.class,codeInsee);
		
		return l;
	}

}
