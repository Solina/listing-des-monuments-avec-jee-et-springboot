package com.fetima.prjetjee.dao;

import java.util.List;

public interface EntityRepository<T> {
	public T get (String id);
	public  T save(T m); // Méthode qui permet d'enregestrer un Monument
	public List<T> findAll();// Méthode qui retourne une liste de Monuments
	public T findOne (String m);// Méthode qui retourne un Monument
	public T update(T m); // méthode qui fait la mise à jour
	public void  delete(String codeM);

}
// Nous avons d'abord créé une interface pour monument, vu que les méthodes generées dans cette interface se repetent pour les 4 classes, nous l'avons '
// convertie à une interface generique qui sera utilisée pour toutes les entités, et nous avons remplacé Monument par T