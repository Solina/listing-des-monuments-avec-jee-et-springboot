package com.fetima.prjetjee.security;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import com.fetima.prjetjee.entities.Utilisateur;

public class AccesSecurity {
	private ArrayList<String> unAutorizedAdmin = new ArrayList<String>();
    private ArrayList<String> unAutorizedVoyagiste = new ArrayList<String>();
    public AccesSecurity() {
    
    unAutorizedAdmin.add("Utilisateurs");
    //Bloquer Voyagiste et Touriste, laisser les autres
	unAutorizedAdmin.add("editDepartement");  //Ok pour le blocage page editer
	unAutorizedAdmin.add("supprimerDepartements");
	
}

public boolean checkAccess(String route, HttpSession session){
    Utilisateur user = (Utilisateur) session.getAttribute("USER");
    if(user==null) return false;
    if(user.getRole().equals("Admin")) {
        if (unAutorizedAdmin.contains(route)) {
            return false;
        }
    }

    if(user.getRole().equals("Voyagiste")) {
        if (unAutorizedVoyagiste.contains(route)) {
            return false;
        }
    }

    return true;
}


}
